# DevOps Challenge

## Part 1 - Load Balancer

- In folder `part1/`
- Start: `docker compose up`
- Open http://localhost:8080 or http://localhost:4430 (ignore cert warning)
- Scale up/down: `docker compose scale app=3`
- Reload browser a few times and see requests routed to different containers

## Part 2 - Tooling

- In folder `part2/`
- Requires `requests` module, see `requirements.txt`
- Set registry user and pass as env: `REGISTRY_USER`, `REGISTRY_PASS`, then run `docker_tags.py`
- Works for a single registry (customize via `REGISTRY_USER`, default to public registry
- Alternatively create a `.env` file and run `docker_tags.sh`
- See additional comments in `docker_tags.py`
