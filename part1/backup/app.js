'use strict';

// backup server, only used when all app workers fail. Sends 503 (service n/a) and a lame excuse

const assert = require('assert');
const express = require('express');
const morgan = require('morgan');
const excuse = require('huh');

assert(process.env.PORT, 'PORT must be set');

const PORT = process.env.PORT;

const app = express();

// ensure we can exit via ctrl-c when in a container
process.on('SIGINT', () => {
    console.log('bye!\n');
    process.exit();
});

app.use(morgan('tiny'));

app.get('/', (req, res) => {
    res.status(503)
        .header('content-type', 'text/plain')
        .send(excuse.get('en'));
});

app.use((req, res) => {
    res.status(404).send('nothing here');
});

app.listen(PORT, () => {
    console.log(':%s', PORT);
});
