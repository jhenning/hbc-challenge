'use strict';

// our highly scalable, non-gmo, gluten-free, billion dollar unicorn app

const assert = require('assert');
const express = require('express');
const morgan = require('morgan');
const os = require('os');

assert(process.env.PORT, 'PORT must be set');

const PORT = process.env.PORT;

const app = express();

// ensure we can exit via ctrl-c when in a container
process.on('SIGINT', () => {
    console.log('*sniff*\n');
    process.exit();
});

app.use(morgan('tiny'));

app.get('/', (req, res) => {
    res.status(200)
        .header('content-type', 'text/plain')
        .send(`Hello, I am container ${os.hostname()}`);
});

app.use((req, res) => {
    res.status(404).send('nothing here');
});

app.listen(PORT, () => {
    console.log(':%s', PORT);
});
