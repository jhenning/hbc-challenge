# docker built in resolver to resolve upstreams
# super short ttl to prevent stale records when scaling up/down
resolver 127.0.0.11 ipv6=off valid=1s;

server {
    server_tokens off;

    listen 80 default_server;
    listen 443 ssl default_server;

    server_name hbc.yodo.io;

    ssl_certificate /etc/nginx/certs/nginx.crt;
    ssl_certificate_key /etc/nginx/certs/nginx.key;

    ssl_session_timeout 1d;
    ssl_session_cache shared:SSL:50m;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS';
    ssl_prefer_server_ciphers on;

    set $mybackend "http://app-group:3000";
    set $mybackup "http://backup:3000";

    error_page 502 @backup;

    location / {
        # using a variable here will force DNS re-resolution w/o needing Nginx Plus, but we
        # can't use an `upstream` (which would give us a lot more to play with)
        proxy_pass $mybackend;
        proxy_connect_timeout 1s; # extra-short timeout for demo

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Request-Uri $request_uri;
    }

    location @backup {
        # poor mans 'server xyz backup', since we cannot use `upstream`
        proxy_pass $mybackup;
    }
}
