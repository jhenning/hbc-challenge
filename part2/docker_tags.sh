#!/bin/bash

if [ -f .env ]; then
  source .env
  export REGISTRY_USER
  export REGISTRY_PASS
  export REGISTRY_URL
fi

python docker_tags.py
