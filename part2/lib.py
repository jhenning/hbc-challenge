import httplib
import socket
import json
import requests
import re
from requests.auth import HTTPBasicAuth, AuthBase


class USocketHTTPConnection(httplib.HTTPConnection):
  """Subclass of HTTPConnection that can connect over a unix socket"""
  
  def __init__(self, path):
    httplib.HTTPConnection.__init__(self, 'localhost')
    self.path = path

  def connect(self):
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.connect(self.path)
    self.sock = sock


class BearerAuth(AuthBase):
  """Auth provider for requests module supporting a Bearer token"""

  def __init__(self, token):
    self.token = token

  def __call__(self, r):
    r.headers['Authorization'] = 'Bearer '+self.token
    return r


class DockerRegistryAPI:
  """Limited implementation of Docker Registry API. Supports authentication using username/
  password and a subset of registry operations."""

  def __init__(self, user, password, api_version='v2', api_url='https://registry.hub.docker.com'):
    self.api_url = api_url
    self.api_version = api_version
    self._user = user
    self._password = password
    self._sessions = {}


  def _get_session(self, base_url):
    """Maintaining a session per host should help speed up multiple requests to the same 
    hosts via connection keep-alive"""
    if base_url not in self._sessions:
      self._sessions[base_url] = requests.Session()
    return self._sessions[base_url]


  def list_image_tags(self, image):
    """List tags for given image"""
    image = self._normalize_image_name(image)
    path = image+'/tags/list'
    return self._call_api(path)

  
  def get_image_manifest(self, image, reference='latest'):
    image = self._normalize_image_name(image)
    path = image+'/manifests/'+reference
    return self._call_api(path)


  def _call_api(self, path='/v2', method='get', token=None, accept=None):
    """Call docker registry API with given method and path. Will attempt to get an auth token
    for the requested operation if prompted so. 
    See https://docs.docker.com/registry/spec/auth/token/ for details on the auth process."""

    auth = BearerAuth(token) if token is not None else None
    headers = {'Accept': 'application/vnd.docker.distribution.manifest.v2+json'}
    url = '/'.join((self.api_url, self.api_version, path))

    session = self._get_session(self.api_url)
    call_method = getattr(session, method)

    r = call_method(url, auth=auth, headers=headers)

    if r.status_code == 401 and token is None:
      # Get token and retry. Condition above will prevent an endless loop
      www_authenticate = r.headers.get('www-authenticate')
      auth = self._parse_www_authenticate(www_authenticate)
      token = self._get_token(auth['realm'], auth['service'], auth['scope'])
      return self._call_api(path, method, token, accept)

    elif r.status_code == 401:
      # Auth failed, but since we had a token, something else went wrong
      www_authenticate = r.headers.get('www-authenticate')
      auth = self._parse_www_authenticate(www_authenticate)
      msg = auth['error'] if 'error' in auth else 'Unspecified authorization error'
      raise ApiError(msg, r.status_code)

    elif r.status_code >= 400:
      raise ApiError('Unexpected response code '+str(r.status_code))

    else:
      return r.json()


  def _parse_www_authenticate(self, www_authenticate):
    """Parse www-authenticate header value for docker auth and return realm, service and scope.
    Will raise an error if auth type is anything else than 'bearer'"""

    # FIXME: might not cover all image name variants
    reg=re.compile('(\w+)[:=] ?"?([\w/:\.-]+)"?')

    type, params = www_authenticate.split(' ')
    
    if type.lower() != 'bearer':
      raise ApiError('Unsupported auth type: '+type)

    return dict(reg.findall(params))


  def _normalize_image_name(self, image_name):
    """ubuntu -> library/ubuntu, etc."""
    return 'library/'+image_name if '/' not in image_name else image_name


  def _get_token(self, realm, service, scope):
    """Request and return an auth token with given scope from auth authenticator"""
    
    auth = HTTPBasicAuth(self._user, self._password)
    params = {"service": service, "scope": scope}

    sess = self._get_session(realm)
    r = sess.get(realm, auth=auth, params=params)

    if r.status_code != 200:
      raise ApiError('Unexpected status while getting token:'+str(r.status_code))

    data = r.json()

    if 'access_token' not in data:
      raise ApiError('Malformed auth response, missing token')
  
    return data['access_token']


class ApiError(Exception):
  """Represents an error when communicating to either Docker Engine API or Docker Registry API"""

  def __init__(self, message, status_code=None):
    self.status_code = status_code
    Exception.__init__(self, message)


class DockerEngineAPI:
  """Implementation of Docker Engine API, can communicate with a local docker engine via
  Socket. Does not support an engine listening on a TCP socket, but should not be hard to
  add"""

  def __init__(self, engine_url='/var/run/docker.sock', api_version='v1.24'):
    self.engine_url = engine_url
    self.api_version = api_version


  def list_containers(self, all=True):
    """Get a list of all running containers"""
    params = []

    if (all == True):
      params.append('all=1')
    
    return self._call_api('/containers/json?'+('&'.join(params)))


  def inspect_image(self, image_name):
    return self._call_api('/images/'+image_name+'/json')


  def _call_api(self, path):
    # Assuming unix socket, more responsible version would check protocol prefix
    conn = USocketHTTPConnection(self.engine_url)
    url = '/'+self.api_version+path
    conn.request('GET', url)

    res = conn.getresponse()

    if res.status != 200:
      raise ApiError('Unexpected response status: '+str(res.status))

    data=json.loads(res.read())
    conn.close()

    return data


def short_digest(digest):
  """Strips the 'sha256' from a docker contant digest and returns the first 12 chars.
  Useful mostly for presentation purposes"""
  return digest.replace('sha256:', '')[0:12]
