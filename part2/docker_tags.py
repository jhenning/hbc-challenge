"""
Tool to compare docker images from running containers with latest image for the
corresponding tag on the public registry. Lists if they match or not. Output
is in CSV format.

Requires REGISTRY_USER and REGISTRY_PASS set as env vars log into the registry. 
Registry url can be set via REGISTRY_URL, uses public registry by default.

Basic algorithm:
 
 - List all containers currently running on the local docker engine
 - For each tag of each container, pull manifest from registry
 - Compare image digest in manifest with local image id
 - If they match, we have the latest image for that container
 - Containers that are not in any registry (local builds) are skipped

Notes about auth:
 
 - Public docker registry requires auth even for pulling public images (why?)
 - Using v2 token auth, rather slow since it requires at least 3 requests for each call -
   one to determine required scope, one to get the token, then repeat initial call
 - Implemenation following https://docs.docker.com/registry/spec/auth/token/
 - Using requests sessions with keep-alive to speed things up
 - Ideas to optimize more:
   - guess required scope w/o making an initial request
   - cache responses for repeated API calls
"""


from lib import DockerRegistryAPI, DockerEngineAPI, short_digest, ApiError
from os import environ as env
import json


if 'REGISTRY_USER' not in env:
  raise Exception('Missing REGISTRY_USER')

if 'REGISTRY_PASS' not in env:
  raise Exception('Missing REGISTRY_PASS')

if 'REGISTRY_URL' not in env:
  registry_url = 'https://registry.hub.docker.com'
else:
  registry_url = env['REGISTRY_URL']


registry_user = env['REGISTRY_USER']
registry_pass = env['REGISTRY_PASS']


registry = DockerRegistryAPI(registry_user, registry_pass, api_url=registry_url)
engine = DockerEngineAPI()


# Print header line
print(','.join((
  'container',
  'tag',
  'local image id',
  'registry image id',
  'is latest'
)))

list = engine.list_containers(all=False)

for container in list:
  image_name = container['Image']
  image = engine.inspect_image(image_name)

  # No repo digest -> not referenced in registry, skip
  if len(image["RepoDigests"]) == 0:
    continue

  container_id = container["Id"][0:11]
  local_img_id = short_digest(image["Id"])

  # When we pulled a newer version of a running containers image, the will
  # containers image will have no tags - simply ignore it
  for tag in image["RepoTags"]:
    name, tag = tag.split(':')

    try:
      manifest = registry.get_image_manifest(name, tag)
      remote_img_id = short_digest(manifest["config"]["digest"])
      match = 'Yes' if image["Id"] == manifest["config"]["digest"] else 'No'
    except ApiError as err:
      remote_img_id = 'err:'+str(err.status_code) if err.status_code else err
      match = 'n/a'

    print(','.join((
      container_id,
      tag,
      local_img_id,
      remote_img_id,
      match
    )))
